import django_filters
from .models import Item


class ItemFilter(django_filters.FilterSet):
    model = Item
    fields = ['category']


